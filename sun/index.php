<?php
    header('Content-Type: image/png');
    $im = imagecreatetruecolor(350, 350);
    $text_color = imagecolorallocate($im, 1, 48, 160);
    $background_color = imagecolorallocate($im, 51,204,255);
    imagefill($im, 0,0, $background_color);
    $sun_color = imagecolorallocate($im, 255,255,0);
    $fill_color = imagecolorallocate($im, 102,255,51);
    imagefilledellipse($im, 175,130, 56, 56, $sun_color);

    imageline($im, 137, 130, 107,130, $sun_color);
    imageline($im, 140, 115, 112,105, $sun_color);
    imageline($im, 154, 100, 133,80, $sun_color);
    imageline($im, 175, 90, 175,60, $sun_color);
    imageline($im, 196, 100, 217,80, $sun_color);
    imageline($im, 210, 115, 238,105, $sun_color);
    imageline($im, 213, 130, 243,130, $sun_color);
    imagestring($im, 5, 125, 170, 'This is sun', $sun_color);

    imagefilledellipse($im, 175,345, 425, 180, $fill_color);
    imagepng($im);
    imagedestroy($im);
?>
