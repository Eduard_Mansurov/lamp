<?php
session_start();
$email = $_POST["email"];
$emailFlag = false;
$unique = true;
$button = $_POST["button"];

$password = $_POST["password"];
$passwordFlag = false;

$sex = $_POST["sex"];
$maleFlag = false;
$femaleFlag = false;
if (($sex == "male") and (isset($sex))) {
    $maleFlag = true;
} elseif (isset($sex)) {
    $femaleFlag = true;
}

$news = $_POST["news"];
$newsFlag = true;
if (!isset($news)) {
    $newsFlag = false;
}

$first = $_POST["first"];
if (!isset($first)) {
    $first = true;
}

 function generateRandomString()
    {
        $length = 14;
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    };

if ($button != 'registration' and $button != 'authorization' and isset($_FILES["fileToUpload"]["name"]) and !isset($_POST['nameFile'])) {
	$rsFile = generateRandomString();
	$target_dir = "uploads/";
	$fileExtension = pathinfo(basename($_FILES["fileToUpload"]["name"]), PATHINFO_EXTENSION);
	$target_file = $target_dir . $rsFile . "." . $fileExtension;
	$uploadOk = 1;
	$imageFileType = pathinfo($target_file, PATHINFO_EXTENSION);
	// Check if image file is a actual image or fake image
	if (isset($_POST["submit"])) {
	    $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
	    if ($check !== false) {
		echo "File is an image - " . $check["mime"] . ".";
		$uploadOk = 1;
	    } else {
		echo "File is not an image.";
		$uploadOk = 0;
	    }
	}

	if ($_FILES["fileToUpload"]["size"] > 5242880) {
    		echo "Sorry, your file is too large.";
    		$uploadOk = 0;
	}

	if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
		&& $imageFileType != "gif" ) {
	    echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.".$imageFileType;
    	$uploadOk = 0;
	}

	if ($uploadOk == 0) {
	    echo "Sorry, your file was not uploaded.";
	// if everything is ok, try to upload file
	} else {
	    if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
		echo "<br>".$_FILES["fileToUpload"]["tmp_name"]." " . $target_file."<br>";
		echo "The file ". basename( $_FILES["fileToUpload"]["name"]). " has been uploaded.";
	    } else {
		echo "Sorry, there was an error uploading your file.";
	    }
	}
}
?>


<html>
<head>
    <title>My pagePHP</title>
    <meta charset="utf-8">
    <link rel="stylesheet" media="screen" href="mystyle.css">
</head>

<body>
<div class="page-wrapper">

    <div class="passwordRules">
        <?php
        if ($_SESSION['isAuth']) {
            echo "Authorized " . "<a href=\"userPage.php\">" . $_SESSION['email'] . "</a>";
        } else {
            echo "Guest";
        }
        ?>
    </div>
    <div class="password-form">
        <form action="index.php" method="POST">
            <p <?php if($_POST['button']=='authorization' and !isset($_SESSION['email'])){echo "";}else{echo "hidden";} ?>>Login or password incorrect</p>
            <p><b>Email: <?php if ($_SESSION['isAuth']) {
                        echo $_SESSION['email'];
                    } ?></b>
                <input maxlength=38 name="email" type=<?php if ($_SESSION['isAuth']) {
                    echo "hidden";
                } else {
                    echo "email";
                } ?> value= <?php echo $email; ?>>
                <?php
                if (strlen($email) < 8 and !$first and ($button == 'registration')) {
                    echo "Email too short";
                } elseif (strlen($email) > 38) {
                    echo "Email too long";
                } elseif (!$first and $button == 'registration') {
                    $file = fopen("users.txt", "r") or die("Unable to open file!");
                    while (!feof($file)) {
                        $arrayData = explode(",", fgets($file));
                        if ($arrayData[1] == $email) {
                            $unique = false;
                            echo "Email not unique";
                            break;
                        }
                    }
                    fclose($file);
                    $emailFlag = true;
                }
                ?>
            </p>

            <p><b>Password: <?php if ($_SESSION['isAuth']) {
                        echo "*******";
                    } ?></b>
                <input maxlenght=14 type=<?php if ($_SESSION['isAuth']) {
                    echo "hidden";
                } else {
                    echo "password";
                } ?> name="password" value= <?php echo $password ?>>
                <?php
                if (strlen($password) < 6 and !$first and $button == 'registration') {
                    echo "Password too short";
                } elseif (strlen($password) > 14) {
                    echo "Password too long";
                } else {
                    $passwordFlag = true;
                }
                ?>

            </p>

            <div <?php if ($_SESSION['isAuth']) {
                echo "hidden";
            } ?>>
                <p><input type="checkbox" name="news" <?php if ($newsFlag) {
                        echo "checked";
                    } ?>>Subscribe to news

                <p>Выберите пол
                    <input type="radio" name="sex" value="male" <?php if ($maleFlag) {
                        echo "checked";
                    } ?>>М
                    <input type="radio" name="sex" value="female" <?php if ($femaleFlag) {
                        echo "checked";
                    } ?> >Ж
                    <?php
                    if (!($maleFlag) and (!$femaleFlag) and !$first and $_POST['button'] != "authorization") {
                        echo " Choose sex";
                    }
                    ?>
                    <input name="first" hidden="true" type="text" value= <?php $first = false;
                    echo $first ?>>
            </div>

            <p><input <?php if ($_SESSION['isAuth']) {
                    echo "hidden";
                } ?> name="button" type="submit" value="registration">
                <input name="button" type="submit" value=<?php if ($_SESSION['isAuth']) {
                    echo "logout";
                } else {
                    echo "authorization";
                } ?> value= <?php echo $password ?>>
		<input name="nameFile" hidden="true" type="text" value= <?php echo $target_file ?>>
        </form>
	
	<div class="uploadFile" <?php if ($_SESSION['isAuth']) {
                echo "hidden";
            } ?>>
        <form action="index.php" method="POST" enctype="multipart/form-data">
            Select image to upload:
            <input type="file" name="fileToUpload" id="fileToUpload">
            <input type="submit" value="UploadImage" name="submit">
        </form>
	</div>



    </div>
    <div class="passwordRules">
        Пароль дожен быть длинной от 6 до 14 символов.
        <p><a href="commentsPage.php">Оставить комментарий</a></p>

        <p><a href="comments.php">Все комментарии</a></p>

        <p><a href="users.php">Пользователи</a></p>
    </div>
</div>
</body>

<?php

if ($emailFlag and $unique and $passwordFlag and ($maleFlag or $femaleFlag) and $button == 'registration') {
    $file = fopen("users.txt", "r") or die("Unable to open file!");
    $id = -1;
    while (!feof($file)) {
        fgets($file);
        $id++;
    }
    fclose($file);
    $file = fopen("users.txt", "a+") or die("Unable to open file!");
    if ($news != "on") {
        $news = "off";
    }
    $rs = generateRandomString();
    $text = $id . "," . $email . "," . $password . "," . $news . "," . $sex . "," . $rs . "," . $_POST['nameFile'] . "\n";
    fwrite($file, $text);
    fclose($file);
}
if ($button == 'authorization' and strlen($email) != 0) {
    $file = fopen("users.txt", "r") or die("Unable to open file!");
    while (!feof($file)) {
        $arrayData = explode(",", fgets($file));
        if ($arrayData[1] == $email and $arrayData[2] == $password) {
            $_SESSION['isAuth'] = true;
            $_SESSION['email'] = $email;
            $_SESSION['secretKey'] = $arrayData[5];
            break;
        }
    }
    fclose($file);
    header('Location: ' . $_SERVER['REQUEST_URI']);
}
if ($button == 'logout') {
    $_SESSION['isAuth'] = false;
    unset($_SESSION['email']);
    unset($_SESSION['secretKey']);
    header('Location: ' . $_SERVER['REQUEST_URI']);
}
?>

<html>


