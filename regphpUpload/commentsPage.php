<?php
session_start();
$first = $_POST["first"];
if (!isset($first)) {
    $first = true;
}
$review = $_POST["review"];
$flagEmail = false;

$file = fopen("users.txt", "r") or die("Unable to open file!");
if (isset($_SESSION['email']) and !$first) {
    while (!feof($file)) {
        $arrayData = explode(",", fgets($file));
        if ($arrayData[1] == $_SESSION['email']) {
            $flagEmail = true;
            $id = $arrayData[0];
        }
    }
}
fclose($file);

function generateRandomString()
    {
        $length = 14;
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    };

if ($_POST['button'] != 'Send' and $_SESSION['isAuth']) {

	$stringFiles="";

	for($i=0; $i<count($_FILES['filesToUpload']['name']); $i++) {
	$rsFile = generateRandomString();
	$fileExtension = pathinfo(basename($_FILES["filesToUpload"]["name"][$i]), PATHINFO_EXTENSION);
	  //Get the temp file path
	  $tmpFilePath = $_FILES['filesToUpload']['tmp_name'][$i];

	  //Make sure we have a filepath
	  if ($tmpFilePath != ""){
	    //Setup our new file path
	    $newFilePath = "uploadFiles/" . $rsFile . "." . $fileExtension;

	    //Upload the file into the temp dir
	    if(move_uploaded_file($tmpFilePath, $newFilePath)) {
		$stringFiles = $stringFiles.$newFilePath."|";
	    }
	  }
	}
}

?>

<html>
<head>
    <title>Comments</title>
    <meta charset="utf-8">
    <link rel="stylesheet" media="screen" href="mystyle.css">
</head>

<body>
<div class="page-wrapper">
    <div class="passwordRules">
        <?php
        if ($_SESSION['isAuth']) {
            echo "Authorized " . $_SESSION['email'];
        } else {
            echo "Guest";
        }
        ?>
    </div>
    <div class="password-form">
        <form action="commentsPage.php" method="POST">
            <p><b>Email: <?php if (isset($_SESSION['email'])) {
                        echo $_SESSION['email'];
                    } else {
                        echo "Guest";
                    } ?></b>
                <?php
                if (!$first and !isset($id) and $_SESSION['isAuth']) {
                    echo "email not found";
                } elseif (!$_SESSION['isAuth'] and !$first) {
                    echo "You must auth";
                }
                ?>
            </p>

            <p><b>Review:</b>
                <textarea maxlength=200 rows=5
                          name="review"><?php echo $review; ?></textarea><?php if (strlen($review) == 0 and (!$first)) {
                    echo "Review is empty";
                } ?>
            </p>

            <input name="first" hidden="true" type="text" value= <?php $first = false;
            echo $first ?>>

            <p><input id="send" type="submit" value="Send" name="button">
	<input name="nameFiles" hidden="true" type="text" value= <?php echo $stringFiles ?>>
        </form>

	<form action="commentsPage.php" method="POST" enctype="multipart/form-data">
		Send these files:<br />
		<input type="file" name="filesToUpload[]" multiple="multiple" /><br />
		<input type="submit" value="SendFiles" name="submit" />
	</form>
	
    </div>
    <div class="passwordRules">
        Пароль дожен быть длинной от 6 до 14 символов.
        <p><a href="commentsPage.html">Оставить комментарий</a></p>

        <p><a href="comments.php">Все комментарии</a></p>

        <p><a href="users.php">Пользователи</a></p>
    </div>
</div>
</body>
<?php
if (isset($id)) {
    $fileU = fopen("users.txt", "r") or die("Unable to open file!");
    while (!feof($fileU)) {
        $arrayData = explode(",", fgets($fileU));
        if ($arrayData[1] == $_SESSION['email'] and $arrayData[5] == $_SESSION['secretKey']) {
            $good = true;
            break;
        }
    }
    fclose($fileU);
}


if (isset($good) and isset($id) and strlen($review) > 0 and strlen($review) < 200 and $_SESSION['isAuth']) {
    $file = fopen("comments.txt", "a+") or die("Unable to open file!");
    $strWithoutN = str_replace(array("\r\n", "\n\r", "\r", "\n"), "<br>", $review);
    $text = $id . "," . $strWithoutN . "," . $_POST['nameFiles'] . "\n";
	echo $text;
    fwrite($file, $text);
}
fclose($file);
?>

<html>
