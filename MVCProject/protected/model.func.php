<?php
function add_new_user($userData){
    $file = fopen("OtherFiles/BD/users.txt", "r") or die("Unable to open file!");
    $id = -1;
    while (!feof($file)) {
      fgets($file);
      $id++;
    }
    fclose($file);
    $file = fopen("OtherFiles/BD/users.txt", "a+") or die("Unable to open file!");
    if ($userData['news'] != "on") {
      $userData['news'] = "off";
    }
    $rs = generateRandomString();
    $text = $id . "," . $userData['email'] . "," . $userData['password'] . "," . $userData['news'] . "," . $userData['sex'] . "," . $rs . "," . "/" . $userData['nameFile'] . "\n";
    fwrite($file, $text);
    fclose($file);
};

function check_unique_email($email)
{
    $unique = true;
    $file = fopen("OtherFiles/BD/users.txt", "r") or die("Unable to open file!");
    while (!feof($file)) {
        $arrayData = explode(",", fgets($file));
        if ($arrayData[1] == $email) {
            $unique = false;
            break;
        }
    }
    fclose($file);
    return $unique;
}
function check_auth($email, $password) {
    $secretKey = '';
    $file = fopen("OtherFiles/BD/users.txt", "r") or die("Unable to open file!");
    while (!feof($file)) {
        $arrayData = explode(",", fgets($file));
        if ($arrayData[1] == $email and $arrayData[2] == $password) {
            $secretKey = $arrayData[5];
            break;
        }
    }
    fclose($file);
    return $secretKey;
}
function get_user($email, $secretKey) {
    $file = fopen("OtherFiles/BD/users.txt", "r") or die("Unable to open file!");
    while (!feof($file)) {
        $arrayData =  explode(",", fgets($file));
        if ($arrayData[1]==$email and $arrayData[5]==$secretKey) {
            $password = $arrayData[2];
            $subscribe = $arrayData[3];
            $sex = $arrayData[4];
            $imgPath  = $arrayData[6];
        }
    }
    fclose($file);
    $userData = array('password' => $password, 'subscribe' => $subscribe, 'sex' => $sex, 'imgPath' => $imgPath);
    return $userData;
}

function get_all_users() {
    $file = fopen("OtherFiles/BD/users.txt", "r") or die("Unable to open file!");
    $arrayAllUsers = '';
    while (!feof($file)) {
        $arrayData = explode(",", fgets($file));
        $arrayAllUsers[] = $arrayData;
    }
    fclose($file);
    return $arrayAllUsers;
}
