<?php
session_start();
function controller_main_index()
{
    $email = $_POST["email"];
    $emailFlag = true;
    $unique = true;
    $button = $_POST["button"];

    $password = $_POST["password"];
    $passwordFlag = false;

    $sex = $_POST["sex"];
    $maleFlag = false;
    $femaleFlag = false;
    if (($sex == "male") and (isset($sex))) {
        $maleFlag = true;
    } elseif (isset($sex)) {
        $femaleFlag = true;
    }

    $news = $_POST["news"];
    $newsFlag = true;
    if (!isset($news)) {
        $newsFlag = false;
    }

    $first = $_POST["first"];
    if (!isset($first)) {
        $first = true;
    }

    $target_file =$_POST['nameFile'];

    if ($button != 'registration' and $button != 'authorization' and isset($_FILES["fileToUpload"]["name"]) and !isset($_POST['nameFile'])) {
        $rsFile = generateRandomString();
        $target_dir = "OtherFiles/uploads/";
        $fileExtension = pathinfo(basename($_FILES["fileToUpload"]["name"]), PATHINFO_EXTENSION);
        $target_file = $target_dir . $rsFile . "." . $fileExtension;
        $uploadOk = 1;
        $imageFileType = pathinfo($target_file, PATHINFO_EXTENSION);
        // Check if image file is a actual image or fake image
        if (isset($_POST["submit"])) {
            $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
            if ($check !== false) {
                //echo "File is an image - " . $check["mime"] . ".";
                $uploadOk = 1;
            } else {
                echo "File is not an image.";
                $uploadOk = 0;
            }
        }

        if ($_FILES["fileToUpload"]["size"] > 5242880) {
            echo "Sorry, your file is too large.";
            $uploadOk = 0;
        }

        if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
            && $imageFileType != "gif" ) {
            echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.".$imageFileType;
            $uploadOk = 0;
        }

        if ($uploadOk == 0) {
            echo "Sorry, your file was not uploaded.";
            // if everything is ok, try to upload file
        } else {
            if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
                //echo "<br>".$_FILES["fileToUpload"]["tmp_name"]." " . $target_file."<br>";
                //echo "The file ". basename( $_FILES["fileToUpload"]["name"]). " has been uploaded.";
            } else {
                echo "Sorry, there was an error uploading your file.";
            }
        }
    }

    //email check
    $errorEmailView = "";
    if (strlen($email) < 8 and !$first and ($button == 'registration')) {
        $errorEmailView = 'email too short';
        $emailFlag = false;
    } elseif (strlen($email) > 38) {
        $emailFlag = false;
        $errorEmailView = 'email too long';
    } elseif (!$first and $button == 'registration' and !check_unique_email($email)) {
        $unique = false;
        $errorEmailView = 'email not unique';
        $emailFlag = false;
    }

    //password check
    $errorPasswordView = "";
    if (strlen($password) < 6 and !$first and $button == 'registration') {
        $errorPasswordView = "Password too short";
    } elseif (strlen($password) > 14) {
        $errorPasswordView = "Password too long";
    } else {
        $passwordFlag = true;
    }

    //check
    if ($emailFlag and $unique and $passwordFlag and ($maleFlag or $femaleFlag) and $button == 'registration') {
        $userData= array('email' => $email, 'password' => $password, 'news' => $news, 'sex' => $sex, 'nameFile' => $_POST['nameFile']);
        add_new_user($userData);
    }
    if ($button == 'authorization' and strlen($email) != 0) {
        $secretKey = check_auth($email, $password);
        if ($secretKey != '') {
                $_SESSION['isAuth'] = true;
                $_SESSION['email'] = $email;
                $_SESSION['secretKey'] = $secretKey;
        }
    }
    if ($button == 'logout') {
        $_SESSION['isAuth'] = false;
        unset($_SESSION['email']);
        unset($_SESSION['secretKey']);
    }


    $data = array('errorEmailView' => $errorEmailView, 'email' => $email, 'password' => $password, 'errorPasswordView' => $errorPasswordView,
        'newsFlag' => $newsFlag, 'maleFlag' => $maleFlag, 'femaleFlag' => $femaleFlag, 'first' => $first, 'target_file' => $target_file);
    return $data;
};

function generateRandomString()
{
    $length = 14;
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
};
